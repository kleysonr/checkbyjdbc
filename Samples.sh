#! /bin/sh
java -cp .:classes12.jar CheckByJdbc -d orcl -H 127.0.0.1 -p 1521 -s orcl -l nagios -x nagios -m f -f sqlfile1.sql -w 80 -c 90 -M "CheckByJdbc Tablespaces" -v 3

java -cp .:classes12.jar CheckByJdbc -d orcl -H 127.0.0.1 -p 1521 -s orcl -l nagios -x nagios -m f -f sqlfile2.sql -w 0 -c 0 -M "CheckByJdbc Invalid Obj" -R "&OWNERLIST='MANAGER','RM'" -v 2

java -cp .:classes12.jar CheckByJdbc -d orcl -H 127.0.0.1 -p 1521 -s orcl -l nagios -x nagios -m f -f sqlfile3.sql -w 300 -c 900 -M "CheckByJdbc LongTimeUserQuery" -v 1

java -cp .:mysql-connector-java-5.1.18-bin.jar CheckByJdbc -d mysql -H 192.168.133.128 -p 3306 -s mysql -l nagios -x nagios -m f -f sqlfile4.sql -w 2 -c 3 -M "CheckByJdbc MySql Sample" -v 2

java -cp .:sqljdbc4.jar CheckByJdbc -d mssql -H 127.0.0.1 -p 1433 -s master -l nagios -x nagios -m f -f sqlfile5.sql -w 2 -c 3 -M "CheckByJdbc MSSQL Sample" -v 2

