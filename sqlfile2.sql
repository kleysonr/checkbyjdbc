select 1 /* WARNING */ nagios_return,
       owner || '.' || object_name nagios_status_msg
from   dba_objects
where  status = 'INVALID'
and    owner in (&OWNERLIST)
order  by owner
