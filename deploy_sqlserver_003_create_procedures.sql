-- =============================================================================
-- Author:		Josemar F. A. Silva
-- Create date: 2013-06-03
-- Description:	Nagos CheckByJdbc plugin procedure sample
-- =============================================================================
USE nagios
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DROP PROCEDURE check_by_jdbc_sample
GO


CREATE PROCEDURE check_by_jdbc_sample
  @num_ret         INT            OUTPUT,
  @str_ret         NVARCHAR(1000) OUTPUT,
  @w_param         INT,
  @c_param         INT,
  @str_adhoc_param NVARCHAR(1000)
AS
BEGIN
  -- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
  SET NOCOUNT ON;
  --
  SET @num_ret = 0;
  SET @str_ret = 'CheckByJdbc-SqlServer procedure: Your input parameter value was "". I have evaluated this and the return to Nagios is !' ;

  /* 
   * Nagios return constants ..
   */
  DECLARE @NAGIOS_RETURN_OK       INT = 0;
  DECLARE @NAGIOS_RETURN_WARNING  INT = 1;
  DECLARE @NAGIOS_RETURN_CRITICAL INT = 2;
  DECLARE @NAGIOS_RETURN_UNKNOWN  INT = 3;  

  /*
   * Put your code here! Your code should evaluate something, considering "w"arning, "c"ritical 
   * parameters and "str_adhoc_param" a string ad-hoc parameter and return on "num_ret" Nagios 
   * STATUS convention and "str_ret" Nagios STATUS return string
   */
  DECLARE @to_char_NAGIOS_RETURN_OK NVARCHAR(100);
  SET     @to_char_NAGIOS_RETURN_OK = CAST( @NAGIOS_RETURN_OK AS CHAR);
  DECLARE @to_char_c_param NVARCHAR(100);
  SET     @to_char_c_param = CAST(@c_param AS CHAR);
  DECLARE @to_char_w_param NVARCHAR(100);
  SET     @to_char_w_param = CAST(@w_param AS CHAR);
  IF @str_adhoc_param = @to_char_NAGIOS_RETURN_OK
    SET @num_ret = @NAGIOS_RETURN_OK;
  ELSE IF @str_adhoc_param = @to_char_c_param
    SET @num_ret = @NAGIOS_RETURN_CRITICAL;
  ELSE IF @str_adhoc_param = @to_char_w_param
    SET @num_ret = @NAGIOS_RETURN_WARNING;
  ELSE
    SET @num_ret = @NAGIOS_RETURN_UNKNOWN;

  SET @str_ret = 'CheckByJdbc-SqlServer Stored Procedure Sample: Your input parameter value was "' + @str_adhoc_param + '". I have evaluated this and the return to Nagios is ' + CAST(@num_ret AS CHAR) + '!' ;
END
GO

