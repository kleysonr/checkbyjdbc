USE nagios
GO

--
-- Test #1: Call Stored Procedure with Nagios "0 - OK" return status
--
DECLARE @num_ret         INT = -1
DECLARE @str_ret         NVARCHAR(1000) = ''
DECLARE @w_param         INT = 1
DECLARE @c_param         INT = 2
DECLARE @str_adhoc_param NVARCHAR(1000) = '0'
EXEC check_by_jdbc_sample @num_ret, @str_ret OUTPUT, @w_param, @c_param, @str_adhoc_param
SELECT @num_ret, @str_ret
GO

--
-- Test #2: Call Stored Procedure with Nagios "1 - WARNING" return status
--
DECLARE @num_ret         INT = -1
DECLARE @str_ret         NVARCHAR(1000) = ''
DECLARE @w_param         INT = 1
DECLARE @c_param         INT = 2
DECLARE @str_adhoc_param NVARCHAR(1000) = '1'
EXEC check_by_jdbc_sample @num_ret, @str_ret OUTPUT, @w_param, @c_param, @str_adhoc_param
SELECT @num_ret, @str_ret
GO

--
-- Test #3: Call Stored Procedure with Nagios "2 - CRITICAL" return status
--
DECLARE @num_ret         INT = -1
DECLARE @str_ret         NVARCHAR(1000) = ''
DECLARE @w_param         INT = 1
DECLARE @c_param         INT = 2
DECLARE @str_adhoc_param NVARCHAR(1000) = '2'
EXEC check_by_jdbc_sample @num_ret, @str_ret OUTPUT, @w_param, @c_param, @str_adhoc_param
SELECT @num_ret, @str_ret
GO

--
-- Test #4: Call Stored Procedure with Nagios "3 - UNKNOWN" return status
--
DECLARE @num_ret         INT = -1
DECLARE @str_ret         NVARCHAR(1000) = ''
DECLARE @w_param         INT = 1
DECLARE @c_param         INT = 2
DECLARE @str_adhoc_param NVARCHAR(1000) = '3'
EXEC check_by_jdbc_sample @num_ret, @str_ret OUTPUT, @w_param, @c_param, @str_adhoc_param
SELECT @num_ret, @str_ret
GO

