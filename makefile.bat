echo off
echo.
echo Compiling ...
echo - CheckByJdbc.java
echo.

javac -classpath .;JSAP-2.0a;com\martiansoftware\jsap\* CheckByJdbc.java


echo.
echo Running ...
echo.
echo - java -cp . CheckByJdbc -h
echo - See some samples of use in 'Samples.bat' or 'Samples.sh' ...
echo.
