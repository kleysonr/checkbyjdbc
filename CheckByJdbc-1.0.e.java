// ----------------------------------------------------------------------------
// CheckByJdbc.java 2013-03-01 josemarsilva version 1.0.e
// ----------------------------------------------------------------------------
//
// Description.: Nagios CheckByJdbc plugin allows you to check a SQL query from
//               a file or a stored procedure on database by a JDBC connection.
//               For CheckByJdbc plugin it doesn't mind what your SQL query
//               does unless it adtopt the following convention:
//               * First column in result set must be number following Nagios
//                 STATUS convention:
//                 - 0     : "OK" status
//                 - 1     : "WARN" status
//                 - 2     : "CRITICAL" status
//                 - others: "UNKNOW" status
//               * Second column in result will be concatenated to Nagios
//                 STATUS string, separated by commas ( <r1c2>; <r2c2>;
//                 ...; <rnc2> )
//               * You can result more than one line. If your query reults more
//                 then one STATUS then plugin return order is "CRITICAL",
//                 "WARN", "UNKNOW", "OK"
//               * Do *not* turn verbose level > 0 running in  production unless
//                 you don't mind for your STATUS message string
//               * You can use built in variables in your sqlfiles:
//                 - '&w': Warning command line parameter
//                 - '&c': Critical command line parameter
//
// Changelog 1.0.e (Kleyson Rios 2014-08-25)
//               * Add support for JDBC on HP Vertica
//
// Changelog 1.0.d (sebgue 2014-05-14)
//               * Fix Perf Data
//
// Changelog 1.0.c (sebgue 2013-03-01)
//               * Add support for Nagios Perf Data
//                 (enabled only when 1 row resultset, and numeric column 2)
//
// Changelog 1.0.b (sebgue 2013-02-27)
//               * Add support for JDBC on IBM DB2/400
//                 (see JTOpen project at jt400.sourceforge.net)
//
// ----------------------------------------------------------------------------
// e-mail......: josemarsilva@yahoo.com.br
// tech-blog...: http://josemarfuregattideabreusilva.blogspot.com.br
// remarks.....: Nagios CheckByJdbc plugin allows you to check a SQL query from
//               a file or a stored procedure on database by a JDBC connection.
//
// ----------------------------------------------------------------------------

import java.sql.*;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.ResultSetMetaData;
import java.sql.ResultSetMetaData;
import java.io.*;
import com.martiansoftware.jsap.*;


public class CheckByJdbc
{
        // ----------------------------------------------------------------------------
        public CheckByJdbc()
        {
        }

        // ----------------------------------------------------------------------------
    public static void main (String args[]) throws Exception
    {
        //
        // Parse command line ...
        //
        JSAP jsap = new JSAP();
        jsap.registerParameter(new Switch("help",'h',"help","Print help message"));

        FlaggedOption   opt1 = new FlaggedOption("dbtype")
                        .setStringParser(JSAP.STRING_PARSER)
                        .setRequired(true)
                        .setShortFlag('d')
                        .setLongFlag(JSAP.NO_LONGFLAG);
                        opt1.setHelp("JDBC database type ['orcl': Oracle, 'mssql': Microsoft SQLServer, 'mysql': MySql, 'db2_400' : DB2/400, 'vertica' : HP Vertica], default is 'orcl'.");
        jsap.registerParameter(opt1);

        FlaggedOption   opt2 = new FlaggedOption("host")
                        .setStringParser(JSAP.STRING_PARSER)
                        .setRequired(true)
                        .setShortFlag('H')
                        .setLongFlag(JSAP.NO_LONGFLAG);
                        opt2.setHelp("Hostname/ip address of database server.");
        jsap.registerParameter(opt2);

        FlaggedOption   opt3 = new FlaggedOption("port")
                        .setStringParser(JSAP.STRING_PARSER)
                        .setDefault("1521")
                        .setRequired(true)
                        .setShortFlag('p')
                        .setLongFlag(JSAP.NO_LONGFLAG);
                        opt3.setHelp("Listener port, default is 1521.");
        jsap.registerParameter(opt3);

        FlaggedOption   opt4 = new FlaggedOption("sid")
                        .setStringParser(JSAP.STRING_PARSER)
                        .setRequired(true)
                        .setShortFlag('s')
                        .setLongFlag(JSAP.NO_LONGFLAG);
                        //opt4.setHelp("[Oracle database SID | MS Sqlserver database name | MySQL database name].");
                        opt4.setHelp("[Oracle database SID | MS Sqlserver database name | MySQL database name | DB2/400 database name | HP Vertica database name].");
        jsap.registerParameter(opt4);

        FlaggedOption   opt5 = new FlaggedOption("login")
                        .setStringParser(JSAP.STRING_PARSER)
                        .setRequired(true)
                        .setShortFlag('l')
                        .setLongFlag(JSAP.NO_LONGFLAG);
                        opt5.setHelp("Database user to connect.");
        jsap.registerParameter(opt5);

        FlaggedOption   opt6 = new FlaggedOption("password")
                        .setStringParser(JSAP.STRING_PARSER)
                        .setRequired(true)
                        .setShortFlag('x')
                        .setLongFlag(JSAP.NO_LONGFLAG);
                        opt6.setHelp("Database user password.");
        jsap.registerParameter(opt6);

        FlaggedOption   opt7 = new FlaggedOption("mode")
                        .setStringParser(JSAP.STRING_PARSER)
                        .setRequired(true)
                        .setShortFlag('m')
                        .setLongFlag(JSAP.NO_LONGFLAG);
                        opt7.setHelp("Mode [f:sqlfile | o:Oracle PL/SQL procedure].");
        jsap.registerParameter(opt7);

        FlaggedOption   opt8 = new FlaggedOption("sqlfile")
                        .setStringParser(JSAP.STRING_PARSER)
                        .setRequired(false)
                        .setShortFlag('f')
                        .setLongFlag(JSAP.NO_LONGFLAG);
                        opt8.setHelp("Full path of sql file to be executed (required on mode sqlfile  \"-m f\").");
        jsap.registerParameter(opt8);

        FlaggedOption   opt9 = new FlaggedOption("stored-procedure")
                        .setStringParser(JSAP.STRING_PARSER)
                        .setRequired(false)
                        .setShortFlag('P')
                        .setLongFlag(JSAP.NO_LONGFLAG);
                        opt9.setHelp("PL/SQL stored procedure to be executed (required on mode PL/SQL procedure \"-m o\").");
        jsap.registerParameter(opt9);

        FlaggedOption   opt10 = new FlaggedOption("add-param")
                        .setStringParser(JSAP.STRING_PARSER)
                        .setRequired(false)
                        .setShortFlag('R')
                        .setLongFlag(JSAP.NO_LONGFLAG);
                        opt10.setHelp("Additional string parameter interpreted differently on each mode."
                                +"\n-m f: <param1>=<value1>;<param2>=<value2>"
                                +"\n-m o: CheckByJdbc_sample( w, c, n_ret, str_ret, add-param) "
                        );
        jsap.registerParameter(opt10);

        FlaggedOption   opt11 = new FlaggedOption("warning")
                        .setStringParser(JSAP.INTEGER_PARSER)
                        .setRequired(true)
                        .setShortFlag('w')
                        .setLongFlag(JSAP.NO_LONGFLAG);
                        opt11.setHelp("Warning value for matched. Normally warning < critical value. If your check want to alert when result < warning , set critical less than warning");
        jsap.registerParameter(opt11);

        FlaggedOption   opt12 = new FlaggedOption("critical")
                        .setStringParser(JSAP.INTEGER_PARSER)
                        .setRequired(true)
                        .setShortFlag('c')
                        .setLongFlag(JSAP.NO_LONGFLAG);
                        opt12.setHelp("Critical value for matched expression. Should be more than Warning value. If not check behaviour is reversed, see warning help text above.");
        jsap.registerParameter(opt12);

        FlaggedOption   opt13 = new FlaggedOption("msg-status")
                        .setStringParser(JSAP.STRING_PARSER)
                        .setRequired(true)
                        .setShortFlag('M')
                        .setLongFlag(JSAP.NO_LONGFLAG);
                        opt13.setHelp("Message status for plugin.");
        jsap.registerParameter(opt13);

        FlaggedOption   opt14 = new FlaggedOption("jdbc-driver")
                        .setStringParser(JSAP.STRING_PARSER)
                        .setRequired(false)
                        .setShortFlag('J')
                        .setLongFlag(JSAP.NO_LONGFLAG);
                        opt14.setHelp("JDBC driver class name. defaults:"
                                +"\n- for Oracle: \"oracle.jdbc.OracleDriver\'"
                                +"\n- for MySQL: \"com.mysql.jdbc.Driver\""
                                +"\n- for MsSQLServer: "
                                +"\n- for DB2/400: \"com.ibm.as400.access.AS400JDBCDriver\""
                                +"\n- for HP Vertica: \"com.vertica.jdbc.Driver\""
                        );
        jsap.registerParameter(opt14);

        FlaggedOption   opt15 = new FlaggedOption("verbose")
                        .setStringParser(JSAP.INTEGER_PARSER)
                        .setDefault("0")
                        .setRequired(false)
                        .setShortFlag('v')
                        .setLongFlag(JSAP.NO_LONGFLAG);
                        opt15.setHelp("Verbose level. [0:no-verbose, 1: low, 2:medium; 3:high].");
        jsap.registerParameter(opt15);

        JSAPResult config = jsap.parse(args);
        boolean isPrintHelp = config.getBoolean("help");

        if (isPrintHelp) {
                System.out.println("Nagios CheckByJdbc v1.0.e plugin allows you to check a SQL query from a file or a stored procedure on database.");
                System.err.println(jsap.getHelp());
                System.err.println("Usage: CheckByJdbc " + jsap.getUsage());
                System.err.println("");
                System.exit(3);
        }

        if (!config.success()) {
                System.err.println();
                System.err.println("Usage: CheckByJdbc " + jsap.getUsage());
                System.err.println();
                //
                // print out specific error messages describing the problems with the command line
                //
                for (java.util.Iterator errs = config.getErrorMessageIterator(); errs.hasNext();) {
                        System.err.println("Error: " + errs.next());
                }
        System.exit(3);
        }
        //
        // Extract parameters ...
        //
        String dbtype           = config.getString("dbtype");
        String ip                       = config.getString("host");
        String port                     = config.getString("port");
        String sid                      = config.getString("sid");
        String username         = config.getString("login");
        String password         = config.getString("password");
        String mode                     = config.getString("mode");
        String sqlfile          = config.getString("sqlfile");
        String stored_proc      = config.getString("stored-procedure");
        String addparam         = config.getString("add-param");
        Integer warning         = new Integer(config.getInt("warning"));
        Integer critical        = new Integer(config.getInt("critical"));
        String msgstatus        = config.getString("msg-status");
        Integer verbose         = new Integer(config.getInt("verbose"));
        String jdbcdriver       = config.getString("jdbc-driver");

        //
        // Check parameters consistency ...
        //
        if ( verbose.intValue() >= 1 ) System.out.println("Debug("+verbose.toString()+"): Check parameter consistency ...");
        if ( verbose.intValue() >= 2 ) {
                System.out.println("Debug("+verbose.toString()+"): dbtype: "+dbtype);
                System.out.println("Debug("+verbose.toString()+"): ip: "+ip);
                System.out.println("Debug("+verbose.toString()+"): port: "+port);
                System.out.println("Debug("+verbose.toString()+"): sid: "+sid);
                System.out.println("Debug("+verbose.toString()+"): username: "+username);
                System.out.println("Debug("+verbose.toString()+"): password: "+password);
                System.out.println("Debug("+verbose.toString()+"): mode: "+mode);
                System.out.println("Debug("+verbose.toString()+"): sqlfile: "+sqlfile);
                System.out.println("Debug("+verbose.toString()+"): stored_proc: "+stored_proc);
                System.out.println("Debug("+verbose.toString()+"): addparam: "+addparam);
                System.out.println("Debug("+verbose.toString()+"): warning: "+warning.toString());
                System.out.println("Debug("+verbose.toString()+"): critical: "+critical.toString());
                System.out.println("Debug("+verbose.toString()+"): msgstatus: "+msgstatus);
                System.out.println("Debug("+verbose.toString()+"): verbose: "+verbose.toString());
                System.out.println("Debug("+verbose.toString()+"): jdbcdriver: "+jdbcdriver);
        }
        boolean bParamConsistency = true;
        String sParamError = "";

        //if ( !( dbtype.equals("orcl") || dbtype.equals("mssql") || dbtype.equals("mysql") ) ) {
        if ( !( dbtype.equals("orcl") || dbtype.equals("mssql") || dbtype.equals("mysql") || dbtype.equals("db2_400") || dbtype.equals("vertica") ) ) {
                bParamConsistency = false;
                //sParamError = "Error: JDBC database type value \"-d\" must be in ['orcl', 'mssql', 'mysqla'].";
                sParamError = "Error: JDBC database type value \"-d\" must be in ['orcl', 'mssql', 'mysqla', 'db2_400', 'vertica'].";
        }
        if ( !(mode.equals("f") || mode.equals("o")) ) {
                bParamConsistency = false;
                sParamError = "Error: Mode value \"-m\" must be in ['f', 'o'].";
        }
        if ( mode.equals("f") && sqlfile == null ) {
                bParamConsistency = false;
                sParamError = "Error: No value specified for option sqlfile parameter \"-f\".";
        }
        if ( mode.equals("o") && stored_proc == null ) {
                bParamConsistency = false;
                sParamError = "Error: No value specified for option plsql-procedure parameter \"-P\".";
        }
        if (!bParamConsistency) {
                System.err.println();
                System.err.println("Usage: CheckByJdbc " + jsap.getUsage());
                System.err.println();
                System.err.println(sParamError);
                System.exit(3); // Critical
        }

        //
        // Build Sql Statement depending on mode ...
        //
        if ( verbose.intValue() >= 1 ) System.out.println("Debug("+verbose.toString()+"): Build Sql Statement ...");
        String sqlstmt = "";
        if ( mode.equals("f") ) {  // f:sqlfile
                if ( addparam == null ) addparam = new String("");
                sqlstmt = new CheckByJdbc().getQueryFromSQLFile(sqlfile, addparam, warning, critical, verbose);
        }
        else if ( mode.equals("o") ) { // o:Oracle PL/SQL stored procedure
                sqlstmt = "begin "+stored_proc+"( ? /* w */, ? /* c */, ? /* n_ret */, ? /* str_ret */, ? /* s_addparam */); end;";
        }
        if ( verbose.intValue() >= 2 ) {
                System.out.println("Debug("+verbose.toString()+"): sqlstmt: "+sqlstmt);
        }

        //
        // Execute check by JDBC connection
        //
        if ( verbose.intValue() >= 1 ) System.out.println("Debug("+verbose.toString()+"): Execute check by JDBC connection  ...");
        if ( jdbcdriver == null ) jdbcdriver = new String("");
        int retStatus = new CheckByJdbc().executeCheckByJdbc(dbtype, ip, port, sid, username, password, mode, sqlstmt, warning, critical, jdbcdriver, msgstatus, verbose);


        //
        // Nagios Plugin required reutrn ...
        //
        if ( verbose.intValue() >= 1 ) System.out.println("Debug("+verbose.toString()+"): Nagios Plugin required return ...");
        if ( verbose.intValue() >= 2 ) {
                System.out.println("Debug("+verbose.toString()+"): retStatus: "+retStatus);
        }
        System.exit(retStatus);
    }


        // ----------------------------------------------------------------------------
        public String getQueryFromSQLFile(String f, String p, Integer warning, Integer critical, Integer verbose)
    {
        if ( verbose.intValue() >= 3 ) {
                System.out.println("Debug("+verbose.toString()+"): getQueryFromSQLFile() f: "+f);
                System.out.println("Debug("+verbose.toString()+"): getQueryFromSQLFile() p: "+p);
        }
        String retStr="";
        String[] paramAndValue = p.split(";");
        String[] replacement;

        try
                {
                        // Concatenate all file lines in one SqlQuery string ...
                        if ( verbose.intValue() >= 2 ) {
                                System.out.println("Debug("+verbose.toString()+"): getQueryFromSQLFile() BufferedReader() ...");
                        }
                BufferedReader in = new BufferedReader(new FileReader(f));
                String str;
                while ((str = in.readLine()) != null) {
                                retStr=retStr+" "+str;
                        }
                in.close();
                        // Replacement parameters for values in SqlQuery string ...
                        if ( verbose.intValue() >= 2 ) {
                                System.out.println("Debug("+verbose.toString()+"): getQueryFromSQLFile() Replacement ...");
                        }
                        if ( !p.equals("") ) {
                                for (int i=0;i<paramAndValue.length; i++) {
                                        if (!paramAndValue[i].equals("")) {
                                                        replacement = paramAndValue[i].split("=");
                                                        if ( replacement.length == 2 )
                                                                if ( !replacement[0].equals("") && !replacement[1].equals("") ) {
                                                                                retStr=retStr.replace(replacement[0],replacement[1]);
                                                                }
                                        }
                                }
                        }
                        retStr=retStr.replace("&c", critical.toString());
                        retStr=retStr.replace("&w", warning.toString());
                }
        catch (IOException ex)
                {
                        ex.printStackTrace();
                }
        return retStr;
    }

        // ----------------------------------------------------------------------------
    public int executeCheckByJdbc( String dbtype, String ip, String port, String sid, String username, String password, String mode, String sqlstmt, Integer warning, Integer critical, String jdbcdriver, String msgstatus, Integer verbose )
        {
        int retStatus=0;
        int nCountStatusOK =0;
        int nCountStatusWARNING = 0;
        int nCountStatusCRITICAL = 0;
        int nCountStatusUNKNOWN = 0;
        int numberOfColumns = 0;
        int numberOfRows = 0;
        String url = "";
        String sMsgStatusOK = "";
        String sMsgStatusWARNING = "";
        String sMsgStatusCRITICAL = "";
        String sMsgStatusUNKNOWN = "";
        String perfDataValue = "";

        //
        // Define url connection string and jdbcdriver ...
        //
        if ( dbtype.equals("orcl") ) {
                url =  "jdbc:oracle:thin:@"+ip+":"+port+":"+sid;
                if ( jdbcdriver.equals("") ) jdbcdriver = "oracle.jdbc.OracleDriver";
        }
        else if ( dbtype.equals("mssql") ) {
                url =  "jdbc:sqlserver://"+ip+":"+port+";"+"databaseName="+sid+";user="+username+";password="+password+";";
                if ( jdbcdriver.equals("") ) jdbcdriver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        }
        else if ( dbtype.equals("mysql") ) {
                url =  "jdbc:mysql://"+ip+":"+port+"/"+sid;
                if ( jdbcdriver.equals("") ) jdbcdriver = "com.mysql.jdbc.Driver";
        }
        else if ( dbtype.equals("db2_400") ) {
                url =  "jdbc:as400://"+ip+":"+port+"/"+sid;
                if ( jdbcdriver.equals("") ) jdbcdriver = "com.ibm.as400.access.AS400JDBCDriver";
        }
        else if ( dbtype.equals("vertica") ) {
                url =  "jdbc:vertica://"+ip+":"+port+"/"+sid;
                if ( jdbcdriver.equals("") ) jdbcdriver = "com.vertica.jdbc.Driver";
        }
        if ( verbose.intValue() >= 2 )
        {
                System.out.println("Debug("+verbose.toString()+"): executeCheckByJdbc() url: " + url);
                System.out.println("Debug("+verbose.toString()+"): executeCheckByJdbc() jdbcdriver: " + jdbcdriver);
        }


        try
                {
                        //
                        // Create JDBC connection, create statement and execute ...
                        //
                        if ( verbose.intValue() >= 2 )
                        {
                                System.out.println("Debug("+verbose.toString()+"): executeCheckByJdbc() Class.forName() ...");
                        }
                        Class.forName(jdbcdriver);
                        if ( verbose.intValue() >= 2 )
                        {
                                System.out.println("Debug("+verbose.toString()+"): executeCheckByJdbc() DriverManager.getConnection() ...");
                        }
                        Connection conn;
                        //if ( dbtype.equals("orcl") || dbtype.equals("mysql") )
                        if ( dbtype.equals("orcl") || dbtype.equals("mysql") || dbtype.equals("db2_400") || dbtype.equals("vertica") )
                                conn = DriverManager.getConnection (url, username, password);
                        else // dbtype.equals("mssql")
                                conn = DriverManager.getConnection (url);
                        if ( verbose.intValue() >= 2 )
                        {
                                System.out.println("Debug("+verbose.toString()+"): executeCheckByJdbc() conn.createStatement() ...");
                        }
                        Statement ps = conn.createStatement();
                        if ( verbose.intValue() >= 2 )
                        {
                                System.out.println("Debug("+verbose.toString()+"): executeCheckByJdbc() ps.executeQuery() ...");
                        }
                        ResultSet rs=ps.executeQuery(sqlstmt);

                        //
                        // Get metadata information for resultset ...
                        //
                        if ( verbose.intValue() >= 2 )
                        {
                                System.out.println("Debug("+verbose.toString()+"): executeCheckByJdbc() rs.getMetaData() ...");
                        }
                        ResultSetMetaData rsmd = rs.getMetaData();
                        //int numberOfColumns = rsmd.getColumnCount();
                        //int numberOfRows = 0;
                        numberOfColumns = rsmd.getColumnCount();
                        numberOfRows = 0;
                        int [] fieldType=new int[numberOfColumns+1];
                        int colNum=0;
                        String cols [] = new String[numberOfColumns+1];
                        for (int i = 1; i <= numberOfColumns; i++) // content column 1-2. element [0] stays untouched
                        {
                                String columnName = rsmd.getColumnName(i);
                                fieldType[i] = rsmd.getColumnType(i);
                                if ( verbose.intValue() >= 2 )
                                {
                                        System.out.println("Debug("+verbose.toString()+"): executeCheckByJdbc() fieldType["+i+"]: "+fieldType[i]);
                                }
                        }
                        if ( verbose.intValue() >= 2 )
                        {
                                System.out.println("Debug("+verbose.toString()+"): executeCheckByJdbc() rs.next()");
                        }
                        while (rs.next()) // each line in result set
                                {
                                        numberOfRows++;
                                        if ( verbose.intValue() >= 3 )
                                        {
                                                System.out.println("Debug("+verbose.toString()+"): executeCheckByJdbc() Row: "+numberOfRows);
                                        }
                                        //if ( fieldType[1] != 2 /* number */ )
                                        if ( fieldType[1] != java.sql.Types.BIGINT
                                          && fieldType[1] != java.sql.Types.INTEGER
                                          && fieldType[1] != java.sql.Types.NUMERIC
                                          && fieldType[1] != java.sql.Types.SMALLINT
                                          && fieldType[1] != java.sql.Types.TINYINT)
                                                {
                                                nCountStatusUNKNOWN = nCountStatusUNKNOWN + 1;
                                                sMsgStatusUNKNOWN = "Plugin message error: First Column must be Nagios number status";
                                                }
                                        else
                                                {
                                                if ( rs.getString(1) == null )
                                                        {
                                                        nCountStatusUNKNOWN = nCountStatusUNKNOWN + 1;
                                                        if ( fieldType[2] != 1 /* string */  )
                                                                if ( rs.getString(2) != null )
                                                                        if ( !rs.getString(2).equals("") )
                                                                                sMsgStatusUNKNOWN = sMsgStatusUNKNOWN + rs.getString(2) + ";" ;
                                                        }
                                                else if ( rs.getString(1).equals("0") )
                                                        {
                                                        nCountStatusOK = nCountStatusOK + 1;
                                                        if ( fieldType[2] != 1 /* string */  )
                                                                if ( rs.getString(2) != null )
                                                                        if ( !rs.getString(2).equals("") )
                                                                                sMsgStatusOK = sMsgStatusOK + rs.getString(2) + ";" ;
                                                        }
                                                else if ( rs.getString(1).equals("1") )
                                                        {
                                                        nCountStatusWARNING = nCountStatusWARNING + 1;
                                                        if ( fieldType[2] != 1 /* string */  )
                                                                if ( rs.getString(2) != null )
                                                                        if ( !rs.getString(2).equals("") )
                                                                                sMsgStatusWARNING = sMsgStatusWARNING + rs.getString(2) + ";" ;
                                                        }
                                                else if ( rs.getString(1).equals("2") )
                                                        {
                                                        nCountStatusCRITICAL = nCountStatusCRITICAL + 1;
                                                        if ( fieldType[2] != 1 /* string */  )
                                                                if ( rs.getString(2) != null )
                                                                        if ( !rs.getString(2).equals("") )
                                                                                sMsgStatusCRITICAL = sMsgStatusCRITICAL + rs.getString(2) + ";" ;
                                                        }
                                                else
                                                        {
                                                        nCountStatusUNKNOWN = nCountStatusUNKNOWN + 1;
                                                        if ( fieldType[2] != 1 /* string */  )
                                                                if ( rs.getString(2) != null )
                                                                        if ( !rs.getString(2).equals("") )
                                                                                sMsgStatusUNKNOWN = sMsgStatusUNKNOWN + rs.getString(2) + ";" ;
                                                        }

                                                // Save value of column 2 for Nagios perf data
                                                if ( numberOfColumns == 2 &&                    // Perf data can be done if there are 2 columns only
                                                        numberOfRows == 1 &&                            // Save result for row 1 only
                                                        rs.getString(2) != null &&
                                                        !rs.getString(2).equals("") &&
                                                        ( fieldType[2] == java.sql.Types.BIGINT ||                      // Perf data can be done with numeric values only
                                                        fieldType[2] == java.sql.Types.DECIMAL ||
                                                        fieldType[2] == java.sql.Types.DOUBLE ||
                                                        fieldType[2] == java.sql.Types.FLOAT ||
                                                        fieldType[2] == java.sql.Types.INTEGER ||
                                                        fieldType[2] == java.sql.Types.NUMERIC ||
                                                        fieldType[2] == java.sql.Types.SMALLINT ||
                                                        fieldType[2] == java.sql.Types.TINYINT ) )
                                                        perfDataValue = rs.getString(2);
                                                else
                                                        perfDataValue = "";

                                                }
                                }
                        if ( verbose.intValue() >= 2 )
                        {
                                System.out.println("Debug("+verbose.toString()+"): executeCheckByJdbc() Number of rows: "+numberOfRows);
                                System.out.println("Debug("+verbose.toString()+"): executeCheckByJdbc() nCountStatusOK: "+nCountStatusOK);
                                System.out.println("Debug("+verbose.toString()+"): executeCheckByJdbc() nCountStatusWARNING: "+nCountStatusWARNING);
                                System.out.println("Debug("+verbose.toString()+"): executeCheckByJdbc() nCountStatusCRITICAL: "+nCountStatusCRITICAL);
                                System.out.println("Debug("+verbose.toString()+"): executeCheckByJdbc() nCountStatusUNKNOWN: "+nCountStatusUNKNOWN);
                                System.out.println("Debug("+verbose.toString()+"): executeCheckByJdbc() sMsgStatusOK: "+sMsgStatusOK);
                                System.out.println("Debug("+verbose.toString()+"): executeCheckByJdbc() sMsgStatusWARNING: "+sMsgStatusWARNING);
                                System.out.println("Debug("+verbose.toString()+"): executeCheckByJdbc() sMsgStatusCRITICAL: "+sMsgStatusCRITICAL);
                                System.out.println("Debug("+verbose.toString()+"): executeCheckByJdbc() sMsgStatusUNKNOWN: "+sMsgStatusUNKNOWN);
                        }
                        //
                        // Close JDBC connection ...
                        //
                        if ( verbose.intValue() >= 2 )
                        {
                                System.out.println("Debug("+verbose.toString()+"): executeCheckByJdbc() ps.close() ...");
                        }
                        ps.close();
                        if ( verbose.intValue() >= 2 )
                        {
                                System.out.println("Debug("+verbose.toString()+"): executeCheckByJdbc() conn.close() ...");
                        }
                        conn.close();
        }
        catch(Exception ex)
        {
                        ex.printStackTrace();
                        retStatus = 3;
                        return retStatus;
        }
        //
        // Required return ...
        //
        if ( nCountStatusCRITICAL > 0 )
                {
                retStatus = 2 /* CRITICAL */;
                msgstatus = "CRITICAL - "+msgstatus;
                if ( !sMsgStatusCRITICAL.equals("") )
                        msgstatus = msgstatus+": "+sMsgStatusCRITICAL;
                }
        else if ( nCountStatusWARNING > 0 )
                {
                retStatus = 1 /* WARNING */;
                msgstatus = "WARNING - "+msgstatus;
                if ( !sMsgStatusWARNING.equals("") )
                        msgstatus = msgstatus+": "+sMsgStatusWARNING;
                }
        else if ( nCountStatusUNKNOWN > 0 )
                {
                retStatus = 3 /* UNKNOWN */;
                msgstatus = "UNKNOWN - "+msgstatus;
                if ( !sMsgStatusUNKNOWN.equals("") )
                        msgstatus = msgstatus+": "+sMsgStatusUNKNOWN;
                }
        else // OK
                {
                retStatus = 0 /* OK */;
                msgstatus = "OK - "+msgstatus;
                if ( !sMsgStatusOK.equals("") )
                        msgstatus = msgstatus+": "+sMsgStatusOK;
                }

                // Concat perf data value to msgstatus
                if ( numberOfColumns == 2 &&                    // Perf data can be done if there are 2 columns only
                        numberOfRows == 1 )                             // Perf data can be done if there is 1 row only
                        {
                        msgstatus = msgstatus + " | SQL_Value=" + perfDataValue + ";" + warning + ";" + critical ;
                        }

        System.out.println(msgstatus);
        return retStatus;
    }

}